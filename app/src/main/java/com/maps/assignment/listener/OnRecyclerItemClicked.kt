package com.maps.assignment.listener

interface OnRecyclerItemClicked {
    fun onItemClick(position: Int, data: Any)
}