package com.maps.assignment.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.maps.assignment.databinding.RowCityNameBinding
import com.maps.assignment.listener.OnRecyclerItemClicked
import com.maps.assignment.models.City

class CityListAdapter(var cityList: ArrayList<City>) :
    RecyclerView.Adapter<CityListAdapter.MyViewHolder>() {

    var onRecyclerItemClicked: OnRecyclerItemClicked? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val binding = RowCityNameBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MyViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.onBind(cityList.get(position), position)
        holder.itemView.setOnClickListener {
            onRecyclerItemClicked?.let { it.onItemClick(position, cityList.get(position)) }
        }
    }

    override fun getItemCount(): Int = cityList.size

    fun setListener(onRecyclerItemClicked: OnRecyclerItemClicked) {
        this.onRecyclerItemClicked = onRecyclerItemClicked
    }

    class MyViewHolder(itemView: RowCityNameBinding) :
        RecyclerView.ViewHolder(itemView.root) {

        val mitemView: RowCityNameBinding = itemView

        fun onBind(city: City, position: Int) {
            mitemView?.txtCityName?.text = city.name.toString()
        }
    }

    fun removeAt(position: Int) {
        cityList.removeAt(position)
        notifyItemRemoved(position)
    }

}