package com.maps.assignment.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.maps.assignment.databinding.WeatherDetailDayRowBinding
import com.maps.assignment.listener.OnRecyclerItemClicked
import com.maps.assignment.models.WeatherDetail
import com.maps.assignment.utils.IconProvider
import java.text.SimpleDateFormat
import java.util.*

class WeatherListAdapter(var weatherList: ArrayList<WeatherDetail>) :
    RecyclerView.Adapter<WeatherListAdapter.MyViewHolder>() {

    var onRecyclerItemClicked: OnRecyclerItemClicked? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val binding =
            WeatherDetailDayRowBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MyViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.onBind(weatherList.get(position), position)
        holder.itemView.setOnClickListener {
            onRecyclerItemClicked?.let { it.onItemClick(position, weatherList.get(position)) }
        }
    }

    override fun getItemCount(): Int = weatherList.size ?: 0

    fun setListener(onRecyclerItemClicked: OnRecyclerItemClicked) {
        this.onRecyclerItemClicked = onRecyclerItemClicked
    }

    class MyViewHolder(itemView: WeatherDetailDayRowBinding) :
        RecyclerView.ViewHolder(itemView.root) {

        val mitemView: WeatherDetailDayRowBinding = itemView

        fun onBind(weatherDeatil: WeatherDetail, position: Int) {

            weatherDeatil?.let {
                mitemView.textViewDay1.setText(getDateTime(it.getDt()))
                var weatherDescription: String? = it?.weather?.get(0)?.main
//                    it.weather?.get(position)?.main

                Glide.with(mitemView.imageViewDay1)
                    .load(IconProvider.getImageIcon(weatherDescription))
                    .into(mitemView.imageViewDay1)

                mitemView.textViewMaxTempDay1.text = it?.main?.tempMax.toString() + "°"
                mitemView.textViewMinTempDay1.text = it?.main?.tempMin.toString() + "°"

            }

        }

        fun getDateTime(timestamp: Long): String {
            try {
                val sdf = SimpleDateFormat("E, dd MMM yyyy \nhh:mm aa")
                val netDate = Date((timestamp) * 1000)
                return sdf.format(netDate)
            } catch (e: Exception) {
                return e.toString()
            }
            return ""
        }

    }

}
