package com.maps.assignment.UI

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.maps.assignment.UI.viewmodels.WeatherActivityViewModel
import com.maps.assignment.adapter.WeatherListAdapter
import com.maps.assignment.databinding.FragmentWeatherDetailBinding
import com.maps.assignment.models.City
import com.maps.assignment.models.Forecast
import com.maps.assignment.models.WeatherDetail
import com.maps.assignment.utils.IconProvider
import com.maps.assignment.utils.Status

class WeatherDetailFragment : Fragment() {

    private var weatherDeatail: ArrayList<WeatherDetail> = ArrayList<WeatherDetail>()
    private lateinit var binding: FragmentWeatherDetailBinding
    private var selCity: City? = null
    private val viewModel: WeatherActivityViewModel by activityViewModels()
    private val adapter: WeatherListAdapter = WeatherListAdapter(weatherDeatail)


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentWeatherDetailBinding.inflate(inflater, container, false)
        initRecyclerView()
        observeData()
        return binding.root
    }

    private fun observeData() {
        //call function to get

        viewModel.selCity.observe(viewLifecycleOwner, {
            selCity = it
            selCity?.let { city ->
                activity?.let { it ->
                    // Call function in viewmodel to get weather deatils
                    viewModel?.callWeatherApiN(cityName = city)

                }
            }
        })

        viewModel?.weatherDataResource()
            ?.observe(this.viewLifecycleOwner, Observer {

                when (it.status) {
                    Status.LOADING -> {
                        binding?.progressbar.visibility = View.VISIBLE
                    }
                    Status.SUCCESS -> {
                        binding?.progressbar.visibility = View.INVISIBLE

                        binding?.contentWeatherPlaceholder.visibility = View.VISIBLE

                        it?.let {
                            showWeatherCard(it?.data)
                            weatherDeatail.addAll(
                                it.data!!.weatherDetailList!!.subList(
                                    1,
                                    it.data.weatherDetailList!!.size
                                )
                            )
                            adapter.notifyDataSetChanged()

                        }
                    }

                    Status.ERROR -> {
                        binding?.progressbar.visibility = View.INVISIBLE
                    }
                }

            })

        binding?.btnBack.setOnClickListener(object : View.OnClickListener {
            override fun onClick(p0: View?) {
                activity?.supportFragmentManager?.popBackStack()
            }
        })

    }



    private fun initRecyclerView() {
        binding.weatherRecyclerView.adapter = adapter
        binding.weatherRecyclerView.hasFixedSize()
    }

    private fun showWeatherCard(forecast: Forecast?) {
        forecast?.let {

            binding?.run {

                detailWeatherCard?.run {
                    textViewCardCityName.text =
                        it.city?.name.toString() + ", " + it.city?.let { "" + it.country }
                    textViewCardWeatherDescription.text =
                        it.weatherDetailList?.get(0)?.weather?.get(0)?.description
                    textViewCardCurrentTemp.text =
                        it.weatherDetailList?.get(0)?.main?.let { it.temp.toString() + "°" }
                    textViewCardMaxTemp.text =
                        it.weatherDetailList?.get(0)?.main?.let { it.tempMax.toString() + "°" }
                    textViewCardMinTemp.text =
                        it.weatherDetailList?.get(0)?.main?.let { it.tempMin.toString() + "°" }
                    textViewHumidity.text =
                        it.weatherDetailList?.get(0)?.main?.let { it.humidity.toString() + "%" }
                    textViewWind.text =
                        it.weatherDetailList?.get(0)?.wind?.let { it.speed.toString() + "m/s" }
                    textViewCloudiness.text =
                        it.weatherDetailList?.get(0)?.clouds?.let { it.all.toString() + "%" }
                    textViewPressure.text =
                        it.weatherDetailList?.get(0)?.main?.let { it.pressure.toString() + "hPa" }

                    var weatherDescription: String? =
                        it.weatherDetailList?.get(0)?.weather?.get(0)?.main

                    Glide.with(this@WeatherDetailFragment)
                        .load(IconProvider.getImageIcon(weatherDescription))
                        .into(imageViewCardWeatherIcon)

                }

            }
        }
    }

}