package com.maps.assignment.UI.viewmodels

import android.util.Log
import androidx.lifecycle.*
import com.maps.assignment.models.City
import com.maps.assignment.models.Forecast
import com.maps.assignment.repository.ProjectRepository
import com.maps.assignment.utils.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class WeatherActivityViewModel(private val repository: ProjectRepository) : ViewModel() {

    val cityListLiveData: LiveData<List<City>>? = repository.getAllCities()
    private var weatherDataResource = MutableLiveData<Resource<Forecast>>()

    private var selCitMutableLiveData = MutableLiveData<City>()
    val selCity: LiveData<City> = selCitMutableLiveData


    fun callWeatherApiN(cityName: City) {
        viewModelScope.launch(Dispatchers.IO) {
            weatherDataResource.postValue(Resource.loading(null))
            try {
                val usersFromApi = repository.callGetWeatherByLatLongN(cityName)
                if (usersFromApi.isSuccessful) {
                    weatherDataResource.postValue(Resource.success(usersFromApi.body()))
                } else {
                    weatherDataResource.postValue(Resource.error("Something Went Wrong", null))
                }

            } catch (e: Exception) {
                e.printStackTrace()
                weatherDataResource.postValue(Resource.error("Something Went Wrong", null))
            }
        }
    }

    fun weatherDataResource(): MutableLiveData<Resource<Forecast>> = weatherDataResource

    fun setCity(selCity: City) {
        selCitMutableLiveData.value = selCity
    }

    fun addCity(city: City) {
        viewModelScope.launch(Dispatchers.IO) { repository.insertCity(city) }
    }

    fun removCity(city: City) {
        viewModelScope.launch(Dispatchers.IO) { repository.removeCity(city) }
    }

    fun deleteAll() {
        viewModelScope.launch(Dispatchers.IO) { repository.removeAllCities() }
    }

    override fun onCleared() {
        super.onCleared()
    }

}

class WeatherViewModelFactory(private val repository: ProjectRepository) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(WeatherActivityViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return WeatherActivityViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}