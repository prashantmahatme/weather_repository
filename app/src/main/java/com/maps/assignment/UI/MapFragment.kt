package com.maps.assignment.UI

import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.constraintlayout.widget.StateSet.TAG
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.maps.assignment.MyApplication
import com.maps.assignment.R
import com.maps.assignment.UI.viewmodels.WeatherActivityViewModel
import com.maps.assignment.UI.viewmodels.WeatherViewModelFactory
import com.maps.assignment.adapter.CityListAdapter
import com.maps.assignment.custom.InternetUtil
import com.maps.assignment.custom.SwipeToDeleteCallback
import com.maps.assignment.databinding.FragmentMapBinding
import com.maps.assignment.listener.OnRecyclerItemClicked
import com.maps.assignment.models.City
import com.maps.assignment.models.Coord
import com.maps.assignment.repository.ProjectRepository
import com.maps.assignment.utils.Constants
import java.util.*
import kotlin.collections.ArrayList

class MapFragment : Fragment(), OnMapReadyCallback, OnRecyclerItemClicked {

    private var cityList: ArrayList<City> = ArrayList<City>();
    private lateinit var binding: FragmentMapBinding
    private var mMap: GoogleMap? = null
    private lateinit var mAdapter: CityListAdapter
    private var mLoc //a marker for your location
            : LatLng? = null

    private var repository: ProjectRepository? = null

    private lateinit var viewModel: WeatherActivityViewModel


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentMapBinding.inflate(inflater, container, false)

        initViewModel()
        initRecyclerView()
        initMap()
        handleBottomSheetEvent()
        observeData()


        binding.fab.setOnClickListener {
            loadWebViewFrag()
        }

        binding.contentHome.btnDeleteAll.setOnClickListener {
            showAlertDialog()
        }

        return binding.root
    }

    private fun showAlertDialog() {

        val builder = AlertDialog.Builder(requireActivity())
        builder.setTitle(R.string.dialogTitle)
        builder.setMessage(R.string.dialogMessage)
        builder.setIcon(android.R.drawable.ic_dialog_alert)
        builder.setPositiveButton("Yes") { dialogInterface, which ->
            viewModel?.deleteAll()
            dialogInterface.dismiss()
        }
        //performing negative action
        builder.setNegativeButton("No") { dialogInterface, which ->
            dialogInterface.dismiss()
        }
        // Create the AlertDialog
        val alertDialog: AlertDialog = builder.create()
        alertDialog.setCancelable(false)
        alertDialog.show()
    }


    private fun loadWebViewFrag() {
        activity?.supportFragmentManager
            ?.beginTransaction()
            ?.replace(R.id.fragment_placeholder, HelpFragment(), "AboutFrag")
            ?.addToBackStack("backstack")
            ?.commit()
    }

    private fun observeData() {
        viewModel?.cityListLiveData?.observe(this.viewLifecycleOwner, androidx.lifecycle.Observer {
            viewModel.cityListLiveData?.value?.let {
                Log.d(TAG, "onCreateView: viewmodel : " + it)
                cityList.clear()
                cityList.addAll(it)
                mAdapter.notifyDataSetChanged()
                updateMap()
            }
        })
    }

    private fun initViewModel() {
        context?.let {
            repository = MyApplication.repository
            repository?.let {
                val factory = WeatherViewModelFactory(it)
                viewModel = ViewModelProvider(
                    requireActivity(),
                    factory
                ).get(WeatherActivityViewModel::class.java)
            }
        }
    }

    private fun updateMap() {
        if (mMap != null) {
            mMap?.clear()
            cityList?.map {
                it.coord?.run {
                    var mlatLong: LatLng? = LatLng(
                        it?.coord?.lat?.toDouble()!!,
                        it.coord?.lon?.toDouble()!!
                    )?.also { latlong ->
                        mMap?.addMarker(
                            MarkerOptions().position(latlong).title(it.name)
                        )?.isDraggable =
                            false
                    }

                }
            }
        }
        if (cityList.size > 0) {
            binding.contentHome.txtNoBookmarked.visibility = View.INVISIBLE
            binding.contentHome.btnDeleteAll.visibility = View.VISIBLE
        } else {
            binding.contentHome.txtNoBookmarked.visibility = View.VISIBLE
            binding.contentHome.btnDeleteAll.visibility = View.GONE
        }

    }


    private fun initRecyclerView() {
        mAdapter = CityListAdapter(cityList).also {
            it.setListener(this@MapFragment)
        }
        binding?.run {
            contentHome?.run {
                recyclerView?.let {
                    it.adapter = mAdapter
                    it.layoutManager = LinearLayoutManager(context)
                    it.setHasFixedSize(true)

                    val swipeHandler =
                        object : SwipeToDeleteCallback(this@MapFragment.requireContext()) {
                            override fun onSwiped(
                                viewHolder: RecyclerView.ViewHolder,
                                direction: Int
                            ) {
                                viewModel.removCity(cityList.get(viewHolder.adapterPosition))
                            }
                        }
                    val itemTouchHelper = ItemTouchHelper(swipeHandler)
                    itemTouchHelper.attachToRecyclerView(recyclerView)

                }
            }
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {

        mMap = googleMap

        mMap?.run {
            // Add a marker in Pune  and move the camera
            var puneLoc = LatLng(18.516726, 73.856255)
            mLoc = puneLoc
            moveCamera(CameraUpdateFactory.newLatLng(puneLoc))

            setOnMapClickListener {
            }

            setOnMapLongClickListener {
                addNewCity(it)
            }

            // Remove marker on long click of info window
            setOnInfoWindowLongClickListener {
                var marker: Marker = it
                cityList.map {
                    if (it.name.equals(marker.title)) {
                        viewModel.removCity(it)
//                    cityList?.remove(it)
                        mAdapter.notifyDataSetChanged()
                    } // remove element from list
                }
                it.remove() // remove from map
            }

            //Drag listener for marker
            setOnMarkerDragListener(object : GoogleMap.OnMarkerDragListener {

                override fun onMarkerDragStart(p0: Marker?) {
                }

                override fun onMarkerDrag(p0: Marker?) {
                }

                override fun onMarkerDragEnd(p0: Marker?) {
                    p0?.position?.run {
                        getAddressFrom(latitude, longitude)
                    }
                }
            })
            updateMap()
        }
    }

    private fun addNewCity(mlatLong: LatLng?) {
        val addresses: List<Address>? = mlatLong?.let { getAddressFrom(it.latitude, it.longitude) }
        addresses?.let {
            val cityName = addresses[0].getAddressLine(0)
//            val stateName = addresses[0].getAddressLine(1)
            val countryName = addresses[0].getAddressLine(2)

            mMap?.addMarker(MarkerOptions().position(mlatLong).title(cityName))?.isDraggable = true

            var city: City = City().apply {
                coord = Coord().apply {
                    this.lat = mlatLong?.latitude.toFloat()
                    this.lon = mlatLong?.longitude.toFloat()
                }
                country = countryName
                name = cityName
            }
            viewModel.addCity(city)

        }

    }


    fun getAddressFrom(latitude: Double, longitude: Double): List<Address>? {
        val geocoder = Geocoder(context, Locale.getDefault())
        try {
            val addresses: List<Address> = geocoder.getFromLocation(latitude, longitude, 1)
            return addresses
        } catch (e: Exception) {

            Toast.makeText(requireContext(), "Unable to get address!", Toast.LENGTH_SHORT).show()
            e.printStackTrace()
        }
        return null
    }

    fun initMap() {
        // Init map
        val mapFragment = childFragmentManager
            ?.findFragmentById(R.id.mapFrag) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    fun handleBottomSheetEvent() {
        binding?.run {
            val bsBehavior: BottomSheetBehavior<View> =
                BottomSheetBehavior.from(contentHome.bottomsheetMap)

            bsBehavior?.also {

                bsBehavior?.state = (BottomSheetBehavior.STATE_COLLAPSED)

                bsBehavior.addBottomSheetCallback(object :
                    BottomSheetBehavior.BottomSheetCallback() {

                    override fun onStateChanged(bottomSheet: View, newState: Int) {
                        if (newState == BottomSheetBehavior.STATE_COLLAPSED) {
                            //action if needed
                            fab.hide()
                        }
                        if (newState == BottomSheetBehavior.STATE_EXPANDED) {
                            fab.show()
                        }
                        if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                        }
                    }

                    override fun onSlide(bottomSheet: View, slideOffset: Float) {
                        val h = bottomSheet.height.toFloat()
                        val off = h * slideOffset
                        when (bsBehavior.getState()) {
                            BottomSheetBehavior.STATE_DRAGGING -> {
                                //reposition marker at the center
                                if (mLoc != null) mMap?.moveCamera(
                                    CameraUpdateFactory.newLatLng(
                                        mLoc
                                    )
                                )
                            }
                            BottomSheetBehavior.STATE_SETTLING -> {
                                //reposition marker at the center
                                if (mLoc != null) mMap?.moveCamera(
                                    CameraUpdateFactory.newLatLng(
                                        mLoc
                                    )
                                )
                            }
                            BottomSheetBehavior.STATE_HIDDEN -> {
                            }
                            BottomSheetBehavior.STATE_EXPANDED -> {
                            }
                            BottomSheetBehavior.STATE_COLLAPSED -> {
                            }
                        }
                    }
                })

            }
        }
    }

    override fun onItemClick(position: Int, data: Any) {
        if (data is City) {
            Log.d(Constants.TAG, "onBindViewHolder: item clicked ")
            val city: City = data as City
            viewModel.setCity(city)
            loadWeatherDetailFrag()
        }
    }

    private fun loadWeatherDetailFrag() {

        if (InternetUtil.isNetworkAvailable(requireContext())) {
            activity?.supportFragmentManager
                ?.beginTransaction()
                ?.replace(R.id.fragment_placeholder, WeatherDetailFragment(), "WeatherFrag")
                ?.addToBackStack("backstack")
                ?.commit()

        } else {
            Toast.makeText(requireContext(), "No internet connection!", Toast.LENGTH_SHORT).show()
        }

    }
}