package com.maps.assignment.UI

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.maps.assignment.databinding.FragmentHelpBinding


class HelpFragment : Fragment() {

    lateinit var binding: FragmentHelpBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentHelpBinding.inflate(inflater, container, false)

        setWebView();
        binding?.btnBack.setOnClickListener(object : View.OnClickListener {
            override fun onClick(p0: View?) {
                activity?.supportFragmentManager?.popBackStack()
            }
        })
        return binding.root
    }

    private fun setWebView() {

        val about = "<!DOCTYPE html>\n" +
                "<html>\n" +
                "<body>\n" +
                "\n" +
                "<h3>Guide to Weather app</h3>\n" +
                "<p>Data provided by \n" +
                "<a href='https://openweathermap.org/'>OpenWeatherMap</a>, under the <a href='http://creativecommons.org/licenses/by-sa/2.0/'>Creative Commons license</a> </p>\n" +
                "\n" +
                "<p>Below screens are the part of the application <p>\n" +
                "<ul>\n" +
                "  <li>Home page</li>\n" +
                "  <li>Weather detail page</li>\n" +
                "</ul>  \n" +
                "\n" +
                "<h4>Home page</h4>\n" +
                "<p>\n" +
                "On home page Map and bookmarked location are shown. To view any added location on map, slide up the bottom page.\n" +
                "</p>\n" +
                "\n" +
                "<h5>Features</h5>\n" +
                "<ul>\n" +
                "  <li>To add any new location,Inside map hold the finger on desired location, marker will be added</li>\n" +
                "  <li>Tap on marker to view location detail</li>\n" +
                "  <li>Hold on marker information popup to remove the location from map</li>\n" +
                "  <li>Bookmarked location can also be deleted by swipe from left to right</li>\n" +
                "  <li>Fab button added to open about app section. This button visible when we slide up the bottom bookmarked location.</li>"+
                "</ul>  \n" +
                "\n" +
                "<h4>Weather detail page</h4>\n" +
                "<p>\n" +
                "You wil be redirected to this page, when you click on any bookmarked location from home page. \n" +
                "</p>\n" +
                "\n" +
                "<h5>Features</h5>\n" +
                "<ul>\n" +
                "  <li>Display today's weather condition and details like temperature, humidity, wind speed and cloudiness.</li>\n" +
                "  <li>Display weather condition for upcoming 5 days for each 3 hour</li>\n" +
                "\n" +
                "</ul>  \n" +
                "\n" +
                "\n" +
                "\n" +
                "</body>\n" +
                "</html>"

        binding.webView.loadData(about, "text/html", "UTF-8")

    }


}