package com.maps.assignment.UI

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.google.android.gms.maps.model.LatLng
import com.maps.assignment.R
import com.maps.assignment.databinding.ActivityHomeBinding


class HomeActivity : AppCompatActivity() {

    private var binding: ActivityHomeBinding? = null
    private val mLoc //a marker for your location
            : LatLng? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_home)

        if (savedInstanceState == null)
            addFragment()
    }

    private fun addFragment() {
        supportFragmentManager.beginTransaction()
            .add(R.id.fragment_placeholder, MapFragment(), "MapFragment").commit()
    }


}