package com.maps.assignment.utils

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}