package com.maps.assignment.utils

import com.maps.assignment.R

class IconProvider {

    companion object{
        fun getImageIcon(weatherDescription: String?): Int {
            val weatherIcon: Int
            weatherIcon = when (weatherDescription) {
                "Thunderstorm" -> R.mipmap.ic_atmosphere
                "Drizzle" -> R.mipmap.ic_drizzle
                "Rain" -> R.mipmap.ic_rain
                "Snow" -> R.mipmap.ic_snow
                "Atmosphere" -> R.mipmap.ic_atmosphere
                "Clear" -> R.mipmap.ic_clear
                "Clouds" -> R.mipmap.ic_cloudy
                "Extreme" -> R.mipmap.ic_extreme
                else -> R.mipmap.ic_launcher
            }
            return weatherIcon
        }

    }

}