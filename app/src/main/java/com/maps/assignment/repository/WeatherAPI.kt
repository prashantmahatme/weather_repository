package com.maps.assignment.repository

import com.maps.assignment.models.Forecast
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherAPI {

    // Sample :   http://api.openweathermap.org/data/2.5/forecast?lat=0&lon=0&appid=fae7190d7e6433ec3a45285ffcf55c86

    @GET("forecast")
    suspend fun getWeatherForecastDataByLatLongN(
        @Query("lat") lat: Float?,
        @Query("lon") lon: Float?,
        @Query("APPID") APIKEY: String?,
        @Query("units") TempUnit: String?,
        @Query("cnt") days: String?
    ): Response<Forecast>

}

