package com.maps.assignment.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.maps.assignment.database.AppDatabase
import com.maps.assignment.models.City
import com.maps.assignment.models.Forecast
import com.maps.assignment.utils.Constants
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProjectRepository() {

    private val apiService: WeatherAPI

    init {
        apiService = ServiceGenerator.createService(WeatherAPI::class.java)
    }

    companion object {
        private const val TAG = "ProjectRepository"
        private var projectRepository: ProjectRepository? = null
        private var database: AppDatabase? = null

        @JvmStatic
        @Synchronized
        fun getInstance(db: AppDatabase): ProjectRepository? {
            if (projectRepository == null) {
                projectRepository = ProjectRepository()
            }
            this.database = db
            return projectRepository
        }
    }


    suspend fun callGetWeatherByLatLongN(cityName: City) =
        apiService.getWeatherForecastDataByLatLongN(
            lat = cityName?.coord?.lat,
            lon = cityName?.coord?.lon,
            APIKEY = Constants.API_KEY,
            TempUnit = Constants.UNITS,
            days = "40"
        )


    suspend fun insertCity(city: City) {
        database?.cityDao()?.insert(city)
    }

    fun getAllCities(): LiveData<List<City>>? {
        return database?.cityDao()?.getAllCities()
    }

    suspend fun removeCity(city: City) {
        database?.cityDao()?.delete(city)

    }

    suspend fun removeAllCities() {
        database?.cityDao()?.deleteAll()
    }


}