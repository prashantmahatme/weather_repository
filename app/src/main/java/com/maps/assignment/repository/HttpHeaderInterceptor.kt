package com.maps.assignment.repository

import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import java.io.IOException

class HttpHeaderInterceptor : Interceptor {
    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {

//        if (Utils.hasActiveInternetConnection())
        val original: Request = chain.request()

        // Request customization: add request headers
        val requestBuilder: Request.Builder = original.newBuilder()
            .addHeader("Cache-Control", "no-cache")
            .addHeader("Cache-Control", "no-store")
        val request: Request = requestBuilder.build()
        return chain.proceed(request)
    }
}