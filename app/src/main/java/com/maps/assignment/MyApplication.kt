package com.maps.assignment

import android.app.Application
import com.maps.assignment.database.AppDatabase
import com.maps.assignment.repository.ProjectRepository

class MyApplication : Application() {

    companion object {
        lateinit var database: AppDatabase
        var repository: ProjectRepository? = null
    }

    override fun onCreate() {
        super.onCreate()
        database = AppDatabase.getDatabase(this)
        repository = ProjectRepository.getInstance(database)

    }


}