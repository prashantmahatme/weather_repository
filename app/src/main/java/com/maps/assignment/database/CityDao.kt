package com.maps.assignment.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.maps.assignment.models.City

@Dao
interface CityDao {

    @Query("select * from city")
    fun getAllCities(): LiveData<List<City>>

    @Insert
    suspend fun insert(city: City)

    @Delete
    suspend fun delete(city: City)

    @Query("delete from city")
    suspend fun deleteAll()


}