package com.maps.assignment.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class WeatherDetail {
    @SerializedName("dt")
    @Expose
    private var dt = 0

    @SerializedName("main")
    @Expose
    var main: Main? = null

    @SerializedName("weather")
    @Expose
    var weather: List<Weather>? = null

    @SerializedName("clouds")
    @Expose
    var clouds: Clouds? = null

    @SerializedName("wind")
    @Expose
    var wind: Wind? = null

    @SerializedName("sys")
    @Expose
    var sys: Sys? = null

    @SerializedName("dt_txt")
    @Expose
    var dtTxt: String? = null

    fun getDt(): Long {
        return dt.toLong()
    }

    fun setDt(dt: Int) {
        this.dt = dt
    }
}