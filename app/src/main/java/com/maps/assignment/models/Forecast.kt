package com.maps.assignment.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Forecast {
    @SerializedName("cod")
    @Expose
    var cod: String? = null

    @SerializedName("message")
    @Expose
    var message = 0f

    @SerializedName("cnt")
    @Expose
    var cnt = 0

    @SerializedName("list")
    @Expose
    var weatherDetailList: List<WeatherDetail>? = null
        private set

    @SerializedName("city")
    @Expose
    var city: City? = null
    fun setDataObject(weatherDetail: List<WeatherDetail>?) {
        weatherDetailList = weatherDetail
    }
}