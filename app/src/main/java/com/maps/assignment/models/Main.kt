package com.maps.assignment.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Main {
    @SerializedName("temp")
    @Expose
    var temp = 0f

    @SerializedName("temp_min")
    @Expose
    var tempMin = 0f

    @SerializedName("temp_max")
    @Expose
    var tempMax = 0f

    @SerializedName("pressure")
    @Expose
    var pressure = 0f

    @SerializedName("sea_level")
    @Expose
    var seaLevel = 0f

    @SerializedName("grnd_level")
    @Expose
    var grndLevel = 0f

    @SerializedName("humidity")
    @Expose
    var humidity = 0

    @SerializedName("temp_kf")
    @Expose
    var tempKf = 0f
}