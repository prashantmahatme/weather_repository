package com.maps.assignment.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Coord() {
    @SerializedName("lat")
    @Expose
    var lat = 0f

    @SerializedName("lon")
    @Expose
    var lon = 0f


}