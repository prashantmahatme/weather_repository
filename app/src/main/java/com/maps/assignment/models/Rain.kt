package com.maps.assignment.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Rain {
    @SerializedName("3h")
    @Expose
    private var _3h = 0f
    fun get3h(): Float {
        return _3h
    }

    fun set3h(_3h: Float) {
        this._3h = _3h
    }
}